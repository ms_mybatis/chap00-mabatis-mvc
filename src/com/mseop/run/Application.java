package com.mseop.run;

public class Application {
	
	/* Mybatis,
	 * 
	 * JDBC에서 좀더 편리하게 사용하기 위해 구현된 것,
	 * MVC ( Model view Controller) 의 줄임말로, 비즈니스 로직을 디자인과 로직부분으로 나누어 개발하는 방식이다. 
	 * 
	 * MVC1구조보다 요즘엔 MVC2 구조를 사용하는 편이니 MVC2 구조로 연습을 진행할것이다.
	 * MVC2는 1보다 유지보수가 간편하고 , MVC1에서는 Controller + view 기능을 모두 담당 했다면
	 * MVC2구조는 view 따로 Controller 따로 각자의 역할을 담당하게 되는 구조이다.
	 * 
	 * 그래서 따로따로 관리하면 유지보수가 하기 한결 수월해진다.
	 * 하지만, 초기 설계 시 시간 및 비용이 많이 든다는 단점이 있다.
	 * 
	 * Model
	 * * 비즈니스 로직을 실행하는 부분이고, 백그라운드에서 동작한다. 데이터를 읽고 쓰는 역할을 담당한다 . 
	 * 
	 * View 
	 * * 화면 출력 부분으로 Controller를 통해 Model에서 처리한 결과를 화면을 출력할 수 있다.
	 * 
	 * Controller
	 * * Voew와 Model을 연결 시켜준다.  Controller는 Servlet으로 구성되어 있다.
	 * 전체적인 흐름을 제어한다.
	 * 
	 * 
	 * DTO(vo)
	 * * DTO는 객체를 정의하는 곳 혹은 생성하는곳을 담당한다.
	 * 
	 * Dao
	 * * Sql을 실행하기 위한 소스
	 * 
	 * Service
	 * * Controller에서 분기처리 혹은 Sql 분기처리
	 * 
	 * Config 
	 * * 실행 설정 파일들
	 * 
	 * Mapper
	 * * .xml을 저장한 파일들
	 * 
	 * ilb
	 * * 외부 라이브러리 파일들 
	 * 
	 * 
	 * 
	 * 
	 * */

}
